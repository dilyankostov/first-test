#!/bin/bash
if type shuf > /dev/null; then
  cowfile="$(cowsay -ln | sed "1 d" | tr ' ' '\n' | shuf -n 1)"
else
  cowfiles=( $(cowsay -ln | sed "1 d") );
  cowfile=${cowfiles[$(($RANDOM % ${#cowfiles[*]}))]}
fi
cowsay -fn "$cowfile"
